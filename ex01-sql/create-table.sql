drop table if exists users;
drop table if exists animals;
drop table if exists orders;
drop table if exists orderDetails;

create table users (
    username VARCHAR(40) NOT NULL,
    password VARCHAR(40) NOT NULL,
    PRIMARY KEY (username)
);

create table animals (
    animalId INTEGER NOT NULL,
    name VARCHAR(40),
    shortDesc TEXT,
    longDesc TEXT,
    imgFilename VARCHAR(40),
    cost INTEGER,
    PRIMARY KEY (animalId)
);

create table orders (
    orderId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    username VARCHAR(40) NOT NULL,
    timestamp TIMESTAMP,
    totalCost INTEGER NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username)
);

create table orderDetails (
    orderId INTEGER NOT NULL,
    animalId INTEGER NOT NULL,
    count INTEGER,
    PRIMARY KEY (orderId, animalId),
    FOREIGN KEY (orderId) REFERENCES orders (orderId),
    FOREIGN KEY (animalId) REFERENCES animals (animalId)
);